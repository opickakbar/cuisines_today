package de.quandoo.recruitment.registry.constant;

public interface Cuisines {
    String ITALIAN_CUISINE = "italian";
    String FRENCH_CUISINE = "french";
    String GERMAN_CUISINE = "german";
}
