package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.constant.Cuisines;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private Map<String, List<Customer>> mapCustomerDatabase = new HashMap<>();

    public InMemoryCuisinesRegistry() {
        mapCustomerDatabase.put(Cuisines.ITALIAN_CUISINE, new ArrayList<>());
        mapCustomerDatabase.put(Cuisines.FRENCH_CUISINE, new ArrayList<>());
        mapCustomerDatabase.put(Cuisines.GERMAN_CUISINE, new ArrayList<>());
    }

    @Override
    public void register(final Customer userId, final Cuisine cuisine) {
        String cuisineName = Optional.ofNullable(cuisine)
                .map(Cuisine::getName)
                .orElseGet(String::new);
        List<Customer> customerDataBasedOnCuisined = mapCustomerDatabase.get(cuisineName);
        if (Objects.isNull(customerDataBasedOnCuisined)) {
            throw new RuntimeException("Unknown cuisine, please reach johny@bookthattable.de to update the code");
        }
        customerDataBasedOnCuisined.add(userId);
        mapCustomerDatabase.put(cuisineName, customerDataBasedOnCuisined);
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        String cuisineName = Optional.ofNullable(cuisine)
                .map(Cuisine::getName)
                .orElseGet(String::new);
        if (cuisineName.isEmpty()) {
            return Collections.emptyList();
        }
        return mapCustomerDatabase.get(cuisineName);
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        List<Cuisine> result = new ArrayList<>();
        List<Customer> italianCustomers = mapCustomerDatabase.get(Cuisines.ITALIAN_CUISINE);
        List<Customer> frenchCustomers = mapCustomerDatabase.get(Cuisines.FRENCH_CUISINE);
        List<Customer> germanCustomers = mapCustomerDatabase.get(Cuisines.GERMAN_CUISINE);
        if (italianCustomers.contains(customer)) {
            result.add(new Cuisine(Cuisines.ITALIAN_CUISINE));
        }
        if (frenchCustomers.contains(customer)) {
            result.add(new Cuisine(Cuisines.FRENCH_CUISINE));
        }
        if (germanCustomers.contains(customer)) {
            result.add(new Cuisine(Cuisines.GERMAN_CUISINE));
        }
        return result;
    }

    /***
     *
     * There is no description for the 'n' parameter here for
     * So, I assume 'n' means the size of minimum customers that cuisine should have.
     * So my solution is basically depends on the n value. if any cuisine's customer reach the size of n, then it will be added to the most popular
     *
     * Example:
     * italian -> 10 customers
     * french -> 8 customers
     * german -> 12 customers
     * topCuisines(10) -> return the list of cuisines that have minimum 10 customers and sort them by descending.
     * so the result would be:
     * 1. german (12 customers)
     * 2. italian (10 customers)
     * => french is not included because it has only 8 customers (less than 10 customers)
     *
     */
    @Override
    public List<Cuisine> topCuisines(final int n) {
        return getCuisineRankedBasedOnHighestCustomer(mapCustomerDatabase, n);
    }

    private List<Cuisine> getCuisineRankedBasedOnHighestCustomer(Map<String, List<Customer>> mapCustomerDatabase, int minimumCustomers) {
        List<Cuisine> result = new ArrayList<>();
        List<Integer> listOfSize = new ArrayList<>();
        int italianCustomersSize = mapCustomerDatabase.get(Cuisines.ITALIAN_CUISINE).size();
        int frenchCustomersSize = mapCustomerDatabase.get(Cuisines.FRENCH_CUISINE).size();
        int germanCustomersSize = mapCustomerDatabase.get(Cuisines.GERMAN_CUISINE).size();
        if (italianCustomersSize >= minimumCustomers) {
            listOfSize.add(italianCustomersSize);
            result.add(new Cuisine(Cuisines.ITALIAN_CUISINE));
        }
        if (frenchCustomersSize >= minimumCustomers) {
            listOfSize.add(frenchCustomersSize);
            result.add(new Cuisine(Cuisines.FRENCH_CUISINE));
        }
        if (germanCustomersSize >= minimumCustomers) {
            listOfSize.add(germanCustomersSize);
            result.add(new Cuisine(Cuisines.GERMAN_CUISINE));
        }
        applyRanking(listOfSize, result);
        return result;
    }

    private void applyRanking(List<Integer> listOfSize, List<Cuisine> listOfCuisines) {
        int n = listOfSize.size();
        for (int i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++) {
                if (listOfSize.get(j) < listOfSize.get(j + 1)) {
                    int temp = listOfSize.get(j);
                    listOfSize.set(j, listOfSize.get(j + 1));
                    listOfSize.set(j + 1, temp);
                    Cuisine tempCuisines = listOfCuisines.get(j);
                    listOfCuisines.set(j, listOfCuisines.get(j + 1));
                    listOfCuisines.set(j + 1, tempCuisines);
                }
            }
    }
}
