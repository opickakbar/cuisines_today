package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Notes: These test cases covered 100% code coverage
 * */
public class InMemoryCuisinesRegistryTest {

    private InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

    @Test
    public void test_registerWithValidCuisine_thenCuisineCustomerShouldBeRegisteredInDatabase() {
        //GIVEN
        Customer customer001 = new Customer("USR-001");
        Customer customer002 = new Customer("USR-002");
        Customer customer003 = new Customer("USR-003");
        Customer customer004 = new Customer("USR-004");
        Cuisine italianCuisine = new Cuisine("italian");
        Cuisine frenchCuisine = new Cuisine("french");
        Cuisine germanCuisine = new Cuisine("german");

        cuisinesRegistry.register(customer001, italianCuisine);
        cuisinesRegistry.register(customer002, frenchCuisine);
        cuisinesRegistry.register(customer003, frenchCuisine);
        cuisinesRegistry.register(customer004, germanCuisine);

        //WHEN
        List<Customer> cuisineCustomers = cuisinesRegistry.cuisineCustomers(frenchCuisine);

        //THEN
        Assert.assertEquals(2, cuisineCustomers.size());
        Assert.assertEquals("USR-002", cuisineCustomers.get(0).getUuid());
        Assert.assertEquals("USR-003", cuisineCustomers.get(1).getUuid());

    }

    @Test(expected = RuntimeException.class)
    public void test_registerWithUnknownCuisine_thenShouldReturnRunTimeException() {
        cuisinesRegistry.register(new Customer("USR-001"), new Cuisine("japanese"));
    }

    @Test
    public void test_registerWithUnknownCuisine_thenShouldReturnExpectedMessageException() {
        try {
            cuisinesRegistry.register(new Customer("USR-001"), new Cuisine("japanese"));
        } catch (RuntimeException e) {
            Assert.assertEquals(e.getMessage(), "Unknown cuisine, please reach johny@bookthattable.de to update the code");
        }
    }

    @Test
    public void test_cuisineCustomers_thenShouldReturnListOfCustomers() {
        //GIVEN
        Customer customer001 = new Customer("USR-001");
        Customer customer002 = new Customer("USR-002");
        Customer customer003 = new Customer("USR-003");
        Cuisine italianCuisine = new Cuisine("italian");

        cuisinesRegistry.register(customer001, italianCuisine);
        cuisinesRegistry.register(customer002, italianCuisine);
        cuisinesRegistry.register(customer003, italianCuisine);

        //WHEN
        List<Customer> customerList = cuisinesRegistry.cuisineCustomers(italianCuisine);

        //THEN
        Assert.assertEquals(3, customerList.size());
        Assert.assertEquals("USR-001", customerList.get(0).getUuid());
        Assert.assertEquals("USR-002", customerList.get(1).getUuid());
        Assert.assertEquals("USR-003", customerList.get(2).getUuid());

    }

    @Test
    public void test_cuisineCustomersWithEmptyCustomer_thenShouldReturnEmptyList() {
        //WHEN
        List<Customer> customerList = cuisinesRegistry.cuisineCustomers(null);

        //THEN
        Assert.assertEquals(0, customerList.size());
    }

    @Test
    public void test_customerCuisines_thenShouldReturnListOfCuisines() {

        //GIVEN
        Customer customer001 = new Customer("USR-001");
        Cuisine italianCuisine = new Cuisine("italian");
        Cuisine frenchCuisine = new Cuisine("french");
        Cuisine germanCuisine = new Cuisine("german");

        cuisinesRegistry.register(customer001, italianCuisine);
        cuisinesRegistry.register(customer001, frenchCuisine);
        cuisinesRegistry.register(customer001, germanCuisine);

        //WHEN
        List<Cuisine> cuisineList = cuisinesRegistry.customerCuisines(customer001);

        //THEN
        Assert.assertEquals(3, cuisineList.size());
        Assert.assertEquals("italian", cuisineList.get(0).getName());
        Assert.assertEquals("french", cuisineList.get(1).getName());
        Assert.assertEquals("german", cuisineList.get(2).getName());
    }

    @Test
    public void test_customerCuisinesWithEmptyCuisines_thenShouldReturnEmptyList() {
        //WHEN
        List<Cuisine> cuisineList = cuisinesRegistry.customerCuisines(null);

        //THEN
        Assert.assertEquals(0, cuisineList.size());
    }

    @Test
    public void test_top3MostPopularCuisines_thenCuisinesListShouldBeSortedDescending() {
        //GIVEN
        Customer customer001 = new Customer("USR-001");
        Customer customer002 = new Customer("USR-002");
        Customer customer003 = new Customer("USR-003");
        Customer customer004 = new Customer("USR-004");
        Customer customer005 = new Customer("USR-005");
        Cuisine italianCuisine = new Cuisine("italian");
        Cuisine frenchCuisine = new Cuisine("french");
        Cuisine germanCuisine = new Cuisine("german");

        cuisinesRegistry.register(customer001, italianCuisine);
        cuisinesRegistry.register(customer002, italianCuisine);
        cuisinesRegistry.register(customer003, italianCuisine);
        cuisinesRegistry.register(customer004, italianCuisine);
        cuisinesRegistry.register(customer001, frenchCuisine);
        cuisinesRegistry.register(customer002, frenchCuisine);
        cuisinesRegistry.register(customer003, frenchCuisine);
        cuisinesRegistry.register(customer001, germanCuisine);
        cuisinesRegistry.register(customer002, germanCuisine);
        cuisinesRegistry.register(customer003, germanCuisine);
        cuisinesRegistry.register(customer004, germanCuisine);
        cuisinesRegistry.register(customer005, germanCuisine);

        //WHEN
        List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(3);

        //THEN
        Assert.assertEquals(3, topCuisines.size());
        Assert.assertEquals("german", topCuisines.get(0).getName());
        Assert.assertEquals("italian", topCuisines.get(1).getName());
        Assert.assertEquals("french", topCuisines.get(2).getName());

    }

    @Test
    public void test_top2MostPopularCuisines_thenCuisinesListShouldBeSortedDescending() {
        //GIVEN
        Customer customer001 = new Customer("USR-001");
        Customer customer002 = new Customer("USR-002");
        Customer customer003 = new Customer("USR-003");
        Customer customer004 = new Customer("USR-004");
        Customer customer005 = new Customer("USR-005");
        Cuisine italianCuisine = new Cuisine("italian");
        Cuisine frenchCuisine = new Cuisine("french");
        Cuisine germanCuisine = new Cuisine("german");

        cuisinesRegistry.register(customer001, italianCuisine);
        cuisinesRegistry.register(customer002, italianCuisine);
        cuisinesRegistry.register(customer001, frenchCuisine);
        cuisinesRegistry.register(customer002, frenchCuisine);
        cuisinesRegistry.register(customer003, frenchCuisine);
        cuisinesRegistry.register(customer003, frenchCuisine);
        cuisinesRegistry.register(customer001, germanCuisine);
        cuisinesRegistry.register(customer002, germanCuisine);
        cuisinesRegistry.register(customer003, germanCuisine);
        cuisinesRegistry.register(customer004, germanCuisine);
        cuisinesRegistry.register(customer005, germanCuisine);

        //WHEN
        List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(4);

        //THEN
        Assert.assertEquals(2, topCuisines.size());
        Assert.assertEquals("german", topCuisines.get(0).getName());
        Assert.assertEquals("french", topCuisines.get(1).getName());
    }

    @Test
    public void test_top1MostPopularCuisines_thenCuisinesListShouldBeSortedDescending() {
        //GIVEN
        Customer customer001 = new Customer("USR-001");
        Customer customer002 = new Customer("USR-002");
        Customer customer003 = new Customer("USR-003");
        Customer customer004 = new Customer("USR-004");
        Customer customer005 = new Customer("USR-005");

        Cuisine italianCuisine = new Cuisine("italian");
        Cuisine frenchCuisine = new Cuisine("french");
        Cuisine germanCuisine = new Cuisine("german");

        cuisinesRegistry.register(customer001, italianCuisine);
        cuisinesRegistry.register(customer002, italianCuisine);
        cuisinesRegistry.register(customer003, italianCuisine);
        cuisinesRegistry.register(customer004, italianCuisine);
        cuisinesRegistry.register(customer005, italianCuisine);
        cuisinesRegistry.register(customer001, italianCuisine);
        cuisinesRegistry.register(customer002, italianCuisine);
        cuisinesRegistry.register(customer003, italianCuisine);
        cuisinesRegistry.register(customer004, italianCuisine);
        cuisinesRegistry.register(customer001, frenchCuisine);
        cuisinesRegistry.register(customer002, frenchCuisine);
        cuisinesRegistry.register(customer003, frenchCuisine);
        cuisinesRegistry.register(customer003, frenchCuisine);
        cuisinesRegistry.register(customer001, germanCuisine);
        cuisinesRegistry.register(customer002, germanCuisine);
        cuisinesRegistry.register(customer003, germanCuisine);
        cuisinesRegistry.register(customer004, germanCuisine);
        cuisinesRegistry.register(customer005, germanCuisine);

        //WHEN
        List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(9);

        //THEN
        Assert.assertEquals(1, topCuisines.size());
        Assert.assertEquals("italian", topCuisines.get(0).getName());
    }

}